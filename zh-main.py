import os
os.environ["CUDA_VISIBLE_DEVICES"] = "3"
from easygui import msgbox, ccbox
import easygui as gui
import json
import random
import torch

from model_utils.Trainer import Trainer
from data_utils.GraphDataGenerator import GraphDataCollector
from data_utils.RankingEvaluator import print_dict
torch.multiprocessing.set_sharing_strategy('file_system')


if __name__ == '__main__':
    config = {
        # data settings
        'thread_num': 4,
        'dataset': 'demo',
        'eval_neg_num': 10,
        'train_neg_num': 4,
        'input_len': 5,
        # training settings
        'train_type': 'train',  # train / eval
        'save_epochs': [400],
        'epoch_num': 50,
        'learning_rate': 0.01,
        'train_batch_size': 10,
        'test_batch_size': 10,
        # graph settings
        # prob settings
        # BERT settings
        'decay_factor': 0.9,
        'hidden_size': 50,
        'initializer_range': 0.1,
        'loss_type': 'pairwise_sample',
        'weight_decay': 0.01,
        'device': torch.device('cuda' if torch.cuda.is_available() else 'cpu'),
    }

random.seed(123)


def main():
    # './datasets/electronics/seq/', './datasets/sports/seq/', './datasets/ml2k/seq/'
    # 0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6,
    # 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0
    data_model = GraphDataCollector(config)
    print_dict(config, 'config')
    trainer = Trainer(config, data_model, save_dir='./datasets/' + config['dataset'] + '/seq/')
    trainer.run_co()
    # zh-code------
    flag = True
    while(flag==True):
        msgbox(msg='\n\n\n\n                      欢迎使用合同条款推荐系统，请选择合同！', title='合同条款推荐系统', ok_button='选择合同', image=None, root=None)
        contract_path = gui.fileopenbox(msg='请选择合同', title='合同审查', default=' ')
        contract_name=contract_path.split('\\')[-1]

        f_f = open('.\\datasets\\res\\file_map_fin.json',encoding='utf-8')
        file_map = f_f.read()
        file_map_dict=json.loads(file_map)
        contract_id=file_map_dict[0][contract_name]
        # predicted_scores=Trainer.predict_by_userId(contract_id)

        # 怎么获得recommend_item_index_list
        # recommend_item_index_list=[]#推荐条款编号列表
        recommend_item_index_list=[1,2,3]#假如推荐条款编号列表是123
        f_i=open('.\\datasets\\res\\item_map_fin.json',encoding='utf-8')
        item_map = f_i.read()
        item_map_dict=json.loads(item_map)
        # item_map_dict_rev = {v:k for k,v in item_map_dict.items()}#反转字典
        recommend_item=''
        for item_index in recommend_item_index_list:
            item = item_map_dict[1][str(item_index)]
            recommend_item=recommend_item+item+'\n'
        flag=ccbox(msg=recommend_item, title='合同条款推荐系统', choices=('继续推荐','退出系统'), image=None)


if __name__ == '__main__':
    main()



