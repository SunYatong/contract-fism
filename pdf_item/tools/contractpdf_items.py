import os
import json
from pdf2txt import main

"""
新的合同所包含的条款
输入：条款集合，以及新合同的地址
输出：[item_id,item_id,item_id...]
"""
#items

def find_file(path):
    # 读条款集合
    with open("item_map_fin.json", encoding="utf-8") as f:  # 条款集合的路径
        item = json.load(f)
    dic = item[1]
    # 合同pdf转化txt
    main([None, path], "pdf2txt.txt")

    item_id =[]
    with open("pdf2txt.txt",'r',encoding='utf-8') as fr:
        lines =fr.read()
    for key, value in dic.items():
        if value in lines:
            item_id.append(int(key))
    return item_id



item_id = find_file('C:/Users/lxj/Desktop/硕士研究/实验室项目/高法/合同分类/借款合同/贷款合同10.pdf') # 所有条款集合，新的合同地址pdf
print(item_id)
