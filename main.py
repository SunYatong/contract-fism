import os
import pickle
os.environ["CUDA_VISIBLE_DEVICES"] = "3"

from easygui import ccbox, fileopenbox, diropenbox
import easygui as gui
import json
from collections import OrderedDict
from data_utils import read_pdf

import random
import torch
from model_utils.Trainer import Trainer
from data_utils.GraphDataGenerator import GraphDataCollector
from data_utils.RankingEvaluator import print_dict
torch.multiprocessing.set_sharing_strategy('file_system')


if __name__ == '__main__':
    config = {
        # data settings
        'thread_num': 4,
        'dataset': 'demo-plus',
        'eval_neg_num': 1500,
        'train_neg_num': 4,
        'input_len': 5,
        # training settings
        'train_type': 'train',  # train / eval
        'epoch_num': 50,
        'learning_rate': 0.001,
        'train_batch_size': 10,
        'test_batch_size': 10,
        # graph settings
        # prob settings
        # BERT settings
        'decay_factor': 0.9,
        'hidden_size': 50,
        'initializer_range': 0.1,
        'loss_type': 'pairwise_sample',
        'weight_decay': 0.01,
        'device': torch.device('cuda' if torch.cuda.is_available() else 'cpu'),
    }

random.seed(123)

def interface (trainer):
    """
    topN_index_list:推荐要素编号列表
    """
    flag = True
    first= True
    while (flag == True):
        if first==True:
            first=False
            with open('contract_recommend.txt', 'r+') as file:
                file.truncate(0)

        if_new = ccbox(msg='\n\n\n\n                      欢迎使用合同要素推荐系统，请选择合同！', title='合同要素推荐系统',
                       choices=('新合同', '已有合同'),
                       image=None)
        # msgbox(msg='\n\n\n\n                      欢迎使用合同要素推荐系统，请选择合同！', title='合同要素推荐系统', ok_button='选择合同', image=None, root=None)


        item_str_list = []

        if if_new:  # 新合同
            contract_pathes = fileopenbox("请选择合同文件", "合同要素推荐", filetypes="*.pdf", multiple=True)
            # contract_pathe = diropenbox(msg='请选择合同文件夹路径，该路径下合同均进行要素推荐处理',title='合同要素推荐',default='')
            # contract_path= gui.fileopenbox(msg='请选择合同', title='合同审查', default=' ')
            for contract_path in contract_pathes:
                # recommend_item_dict = OrderedDict()
                contract_name = contract_path.split('\\')[-1]
                item_str_list = read_pdf.read_contract(contract_path)  # 新合同分割出的要素/已有要素
                top_rank_item_strs = trainer.pred_for_new_user(contract_path, item_str_list=item_str_list)#推荐的topk要素
                # recommend_item_dict[contract_name]=top_rank_item_strs
                recommend_item_str = '#########################################################\n'
                recommend_item_str += '合同名称：'+contract_name+'\n'+ '已有要素：'+str(item_str_list)+'\n'+'推荐要素：'+str(top_rank_item_strs)+'\n'
                with open('contract_recommend.txt', 'a') as f:
                    f.write(recommend_item_str)

        else:  # 训练数据集中的合同
            # contract_name = contract_path.split('\\')[-1]
            # f_f = open('.\\datasets\\res\\file_map_fin.json', encoding='utf-8')
            # file_map = f_f.read()
            # file_map_dict = json.loads(file_map)
            # contract_id = file_map_dict[0][contract_name]
            # ---------------
            contract_ids = gui.enterbox(msg='输入合同 ID，请用“+”间隔', title=' ', default='', strip=True, image=None, root=None)
            contract_id_list = contract_ids.split('+')
            for contract_id in contract_id_list:
                # recommend_item_dict = {}
                top_rank_item_strs = trainer.predict_by_userId(userId=contract_id)
                # recommend_item_dict[contract_name] = top_rank_item_strs
                # 用已有要素变量替换item_str_list
                items_ids = trainer.data_model.userId2itemIds[contract_id]
                print("items_ids",items_ids)
                # print("itemIdx2Str",trainer.data_model.itemIdx2Str)
                items_idxs = [trainer.data_model.itemId2Idx[item_id] for item_id in items_ids]
                item_str_list = [trainer.data_model.itemIdx2Str[item_idx] for item_idx in items_idxs]
                print("item_str_list",item_str_list)
                recommend_item_str = '#########################################################\n'
                recommend_item_str += '合同id：'+contract_id+'\n'+ '已有要素：'+str(item_str_list)+'\n'+'推荐要素：'+str(top_rank_item_strs)+'\n'
                with open('contract_recommend.txt', 'a') as f:
                    f.write(recommend_item_str)

        recommend_item = ''
        if top_rank_item_strs == 0:
            recommend_item = 'all new clauses'
        else:
            recommend_item='合同推荐要素文件contract_recommend.txt已生成，请及时查看！'
            # for item_str in top_rank_item_strs:
            #     recommend_item = recommend_item + item_str + '\n'

        flag = ccbox(msg=recommend_item, title='合同要素推荐系统', choices=('继续推荐', '退出系统'), image=None)

def main():
    # './datasets/electronics/seq/', './datasets/sports/seq/', './datasets/ml2k/seq/'
    # 0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6,
    # 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0
    data_model = GraphDataCollector(config)
    print_dict(config, 'config')
    trainer = Trainer(config, data_model, save_dir='./datasets/' + config['dataset'] + '/seq/')
    trainer.run_co()
    # trainer.predict_by_userId(userId='2', topN=10)
    interface(trainer)

if __name__ == '__main__':
    main()



