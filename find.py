import fitz
import os
import json
import random
import string
def _parse_highlight(annot, wordlist):
    points = annot.vertices
    #print(points)
    quad_count = int(len(points) / 4)
    sentences = ['' for i in range(quad_count)]
    for i in range(quad_count):
        r = fitz.Quad(points[i * 4: i * 4 + 4]).rect
        words = [w for w in wordlist if fitz.Rect(w[:4]).intersects(r)]
        sentences[i] = ' '.join(w[4] for w in words)
    sentence = ' '.join(sentences)
    return sentence

filePath = 'C:\\Users\\lxj\\Desktop\\合同数据集1' #原始的合同文件
outPath = 'C:\\Users\\lxj\\Desktop\\preprocess' #处理后的文件

all_pairs=[] #[文件名，条款名]

for k in os.listdir(filePath):

    mupdf_doc = fitz.open(os.path.join(filePath,k))
    pages = mupdf_doc.page_count
    # for mupdf_page in mupdf_doc:
    for page in range(pages):
        mupdf_page = mupdf_doc.load_page(page)
        wordlist = mupdf_page.get_text("words")  # list of words on page
        wordlist.sort(key=lambda w: (w[3], w[0]))  # ascending y, then x
        #print(wordlist)
        for annot in mupdf_page.annots():
            # underline / highlight / strikeout / squiggly : 8 / 9 / 10 / 11
            if annot.type[0] == 8:
                item=_parse_highlight(annot, wordlist)
                all_pairs.append([k,item])

#写入文件
with open(os.path.join(outPath,"res.txt"),"w", encoding='utf-8') as f:
    print(all_pairs,file=f)

#读出文件

def list2map(ori_list):
    '''
    ori_list:[a,a,b,c,d]

    output:  ori2id:{a:0,b:1}  id2ori:{0:a,1:b}
    '''
    ori_list=list(set(ori_list))
    index_list=list(range(len(ori_list)))
    return dict(zip(ori_list,index_list)),dict(zip(index_list,ori_list))

with open(os.path.join(outPath,"doc_item.txt"),"r",encoding='utf-8') as f:
    all_pairs=eval(f.readline())

new_item = []
old_item = []
with open(os.path.join(outPath,"new_items.txt"),"r",encoding='utf-8') as f:
    for item in f.readlines():
        new_item.append(item.strip())

with open(os.path.join(outPath,"old_items.txt"),"r",encoding='utf-8') as f:
    for item in f.readlines():
        old_item.append(item.strip())

new_item_pairs = {} #{'旧的条款名'：’新的条款明‘}
for i in range(len(old_item)):
    new_item_pairs[old_item[i]] = new_item[i]

new_all = [] #不包含'1'条款的新的二元组
for i in range(len(all_pairs)):
    all_pairs[i][1] = new_item_pairs[all_pairs[i][1]]
    if all_pairs[i][1] !='1':
        new_all.append(all_pairs[i])

all_pairs=new_all

#构建字典： {合同名：[无重复的条款名]}
file2item={}
for file,item in all_pairs:
    if file not in file2item:
        file2item[file]=[]
    if item not in file2item[file]:
        file2item[file].append(item)

#根据规则构建 test dev中的条款
new_ex={}
for file in file2item:
    if len(new_ex)>30:
        break
    if len(file2item[file])>15:
        new_file=file.split('.')[0]+random.choice(string.ascii_letters)+".pdf"
        new_num=random.randint(1,10)
        new_ex[new_file]=file2item[file][:new_num]


#输入：new_ex，file2item -> file_list, item_list
#输出：file2id,id2file,item2id,id2item
file_list=list(file2item)+list(new_ex)
new_item=set(new_item)
new_item.remove('1')
item_list=list(new_item)

file2id,id2file=list2map(file_list)
item2id,id2item=list2map(item_list)

#把文件名和项目名的id映射存储为json文件
for data,path in zip([[file2id,id2file],[item2id,id2item]],['file_map_fin.json','item_map_fin.json']):
    with open(os.path.join(outPath,path),"w",encoding='utf-8') as f:
        json.dump(data,f,indent=4,ensure_ascii=False)


#根据file2item生成train  根据new_ex生成test dev
#根据 字典 生成 [[file_id,item_id,1.0],[..]] 再存入文件
def get_dataset(f2t,out_file_name):
    with open(out_file_name,"w",encoding='utf-8') as f:
        for file_name in f2t:
            for item in f2t[file_name]:
                print('%d %d 1.0'%(file2id[file_name],item2id[item]),file=f)
get_dataset(file2item,os.path.join(outPath,"train_fin.txt"))

other_file_name = list(new_ex)
random.shuffle(other_file_name)
test_file_name = other_file_name[:15]
dev_file_name = other_file_name[15:]

def get_dev_test(some_file_name):
    new_data={}
    for name in some_file_name:
        new_data[name]=new_ex[name]
    return new_data

test_data=get_dev_test(test_file_name)
dev_data=get_dev_test(dev_file_name)

get_dataset(test_data,os.path.join(outPath,"test_fin.txt"))
get_dataset(dev_data,os.path.join(outPath,"dev_fin.txt"))

print()

file2id,id2file={},{} #{"劳动合同.pdf":0,..} {0:"劳动合同.pdf"}
item2id,id2item={},{} #{"劳动报酬":0,..} {0:"劳动报酬"，..}

def add_dict(a, text2id, id2text):
    # 在两个字典中加入元素a
    if a not in text2id:
        text2id[a] = len(text2id)
        id2text[text2id[a]] = a


for file,item in new_all:
    add_dict(file,file2id,id2file)
    add_dict(item,item2id,id2item)

#把文件名和项目名的id映射存储为json文件
for data,path in zip([[file2id,id2file],[item2id,id2item]],['file_map.json','item_map.json']):
    with open(os.path.join(outPath,path),"w",encoding='utf-8') as f:
        json.dump(data,f,indent=4,ensure_ascii=False)


#将新的二元组存储进文件 new_doc_item.txt
with open(os.path.join(outPath,'new_doc_item.txt'),"w",encoding="utf-8") as f:
    print(new_all,file=f)


file2id,id2file={},{} #{"劳动合同.pdf":0,..} {0:"劳动合同.pdf"}
item2id,id2item={},{} #{"劳动报酬":0,..} {0:"劳动报酬"，..}

def add_dict(a, text2id, id2text):
    # 在两个字典中加入元素a
    if a not in text2id:
        text2id[a] = len(text2id)
        id2text[text2id[a]] = a


for file,item in new_all:
    add_dict(file,file2id,id2file)
    add_dict(item,item2id,id2item)

#把文件名和项目名的id映射存储为json文件
for data,path in zip([[file2id,id2file],[item2id,id2item]],['file_map.json','item_map.json']):
    with open(os.path.join(outPath,path),"w",encoding='utf-8') as f:
        json.dump(data,f,indent=4,ensure_ascii=False)


############
# item2num={}
# for _,item in new_all:
#     if item not in item2num:
#         item2num[item]=0
#     item2num[item]+=1
#
# num2item={} #1:[]  2:[] 有 2253 个条款只出现了1次
# for item in item2num:
#     num=item2num[item]
#     if num not in num2item:
#         num2item[num]=[]
#     num2item[num].append(item)



#输入：file2id,item2id, new_all
#输出：train,dev,test 形式是 文件id 条款id 1.0
all_pairs=new_all
train_file=[]
dev_file=[]
test_file=[]
all_dataset=[]


#总样本数量:len(file2id) 按照8 1 1的比例随机划分
n=len(file2id)
index_list=list(range(0,n)) #文件id
random.shuffle(index_list)
n_dev=int(n*0.1)
n_test=int(n*0.1)

for ex in all_dataset:
    if ex[0] in index_list[0:n_dev]:
        dev_dataset.append(ex)
    elif ex[0] in index_list[n_dev:(n_test+n_dev)]:
        test_dataset.append(ex)
    else:
        train_dataset.append(ex)
print()

#总样本数量:len(file2id) 按照8 1 1的比例随机划分
n=len(file2id)
index_list=list(range(0,n)) #文件id
random.shuffle(index_list)
n_dev=int(n*0.1)+2
n_test=int(n*0.1)+2

for ex in all_dataset:
    if ex[0] in index_list[0:n_dev]:
        dev_dataset.append(ex)
    elif ex[0] in index_list[n_dev:(n_test+n_dev)]:
        test_dataset.append(ex)
    train_dataset.append(ex)

for data,path in zip([train_dataset,dev_dataset,test_dataset],['train.txt','dev.txt','test.txt']):
    with open(os.path.join(outPath,path),"w") as f:
        for ex in data:
            print('%d %d %.1f'%(ex[0],ex[1],ex[2]),file=f)

#委托人:{
#    委托人，
#   会损害委托人在，
#    委托人声明
#}

#【所有的条款】