from data_utils.RankingEvaluator import RankingEvaluator
import torch
from torch import optim
import time
import os
import numpy as np
from torch.optim.lr_scheduler import ReduceLROnPlateau
from model.GraphSeq import ProbGraphSeqRec
from model.FISM import FISM
import fitz


class Trainer:
    def __init__(self, config, data_model, save_dir):

        train_loader = data_model.generate_train_dataloader()
        test_loader = data_model.generate_test_dataloader()

        self.config = config
        self.save_dir = save_dir
        self.train_type = config['train_type']
        self.rec_model = 'FISM'

        self.train_loader = train_loader
        self._evaluator_1 = RankingEvaluator(test_loader)
        self.item_dist = np.array(data_model.item_dist)
        self.user_dist = np.array(data_model.user_dist)
        self.train_size = len(train_loader.dataset)
        self.model_save_dir = './datasets/' + self.config['dataset'] + '/model/'
        self.model_save_path = self.model_save_dir + self.rec_model + '-'
        self.data_model = data_model
        rec_model_1 = ProbGraphSeqRec(config, FISM(config))
        self._model_1 = rec_model_1
        self._device = config['device']

        self.userIdx2target_num = data_model.userIdx2target_num

        if self.train_type == 'train':

            self._model_1.double().to(self._device)
            self._optimizer_1 = _get_optimizer(
                self._model_1, learning_rate=config['learning_rate'], weight_decay=config['weight_decay'])
            self.scheduler_1 = ReduceLROnPlateau(self._optimizer_1, 'max', patience=10,
                                                 factor=config['decay_factor'])
            self.forget_rates = self.build_forget_rates()

        elif self.train_type == 'eval':
            self._model_1.set_encoder(self.load_model())
            self._model_1.double().to(self._device)
            self._evaluator_1.evaluate(model=self._model_1, train_iter=0, userIdx2target_num=self.userIdx2target_num)

    def build_forget_rates(self):
        forget_rates = np.ones(self.config['epoch_num']) * 0.06
        forget_rates[:1] = np.linspace(0, 0.06, 1)
        return forget_rates

    def save_model(self):
        if not os.path.exists(self.model_save_dir):
            os.makedirs(self.model_save_dir)
        save_path = self.model_save_path + '-model.pkl'
        torch.save(self._model_1.context_encoder, save_path)
        print(f'model saved at {save_path}')

    def load_model(self):
        load_path = self.model_save_path + '-model.pkl'
        print(f'loading model from {load_path}')
        return torch.load(load_path)

    def co_train_one_batch(self, batch, epoch_num, batch_num):
        self._model_1.train()
        self._optimizer_1.zero_grad()
        # [bs], [bs], [bs]
        overall_loss_1, sample_idices = self._model_1(batch)

        overall_loss_1.backward()
        self._optimizer_1.step()

        return overall_loss_1

    def run_co(self):
        if self.train_type == 'train':
            print('=' * 60, '\n', 'Start Training', '\n', '=' * 60, sep='')
            keep_train = True
            self.evaluate(-1)
            for epoch in range(self.config['epoch_num']):
                start = time.time()
                loss_1_iter = 0
                for i, batch in enumerate(self.train_loader):
                    loss_1 = self.co_train_one_batch(batch, epoch, i)
                    loss_1_iter += loss_1.item()
                print(f'################## epoch {epoch} ###########################')
                print(f"loss: {round(loss_1_iter / len(self.train_loader), 4)}, len_train_loader:{len(self.train_loader)}")
                keep_train = self.evaluate(epoch)
                print('#########################################################')

                if not keep_train:
                    break
            self.save_model()

    def evaluate(self, iter):
        self._model_1.eval()
        keep_train_1, ndcg10_1 = self._evaluator_1.evaluate(model=self._model_1, train_iter=iter, userIdx2target_num=self.userIdx2target_num)
        self.scheduler_1.step(ndcg10_1)

        return keep_train_1

    def predict_by_userId(self, userId, topN=5):
        userIdx = self.data_model.userId2Idx[userId]
        print('-----------')
        print(f'documentId: {userId}')
        return self.predict_by_userIdx(userIdx, topN)

    # 读每一个pdf高亮
    def _parse_highlight(self, annot, wordlist):
        points = annot.vertices
        # print(points)
        quad_count = int(len(points) / 4)
        sentences = ['' for i in range(quad_count)]
        for i in range(quad_count):
            r = fitz.Quad(points[i * 4: i * 4 + 4]).rect
            words = [w for w in wordlist if fitz.Rect(w[:4]).intersects(r)]
            sentences[i] = ' '.join(w[4] for w in words)
        sentence = ' '.join(sentences)
        return sentence

    def generate_item_str_list(self, contractPDF):
        item_str_list = []   # [条款名，... ，条款名]
        # filePath = 'C:\\Users\\lxj\\Desktop\\合同数据集'  # 原始的合同文件 目录需要改
        # 读一个文件
        mupdf_doc = fitz.open(contractPDF) #看情况修改
        pages = mupdf_doc.page_count
        # for mupdf_page in mupdf_doc:
        for page in range(pages):
            mupdf_page = mupdf_doc.load_page(page)
            wordlist = mupdf_page.get_text("words")  # list of words on page
            wordlist.sort(key=lambda w: (w[3], w[0]))  # ascending y, then x
            # print(wordlist)
            for annot in mupdf_page.annots():
                # underline / highlight / strikeout / squiggly : 8 / 9 / 10 / 11
                if annot.type[0] == 8:
                    item = self._parse_highlight(annot, wordlist)
                    item_str_list.append(item)
        return item_str_list

    def pred_for_new_user(self,contract_path, item_str_list, topN=5):
        input_items = []
        for item_str in item_str_list:
            if item_str in self.data_model.itemStr2Idx:
                input_items.append(self.data_model.itemStr2Idx[item_str])
        if len(input_items) == 0:
            print(f'all new clauses for {contract_path}')
            return 0
        append_list = [0] * (self.data_model.input_len - len(input_items))
        input_masks = [1] * len(input_items) + append_list
        targets = list(set(range(1, self.data_model.numItem)) - set(input_items))

        input_items_tensor = torch.tensor([input_items + append_list])
        input_masks_tensor = torch.tensor([input_masks])
        targets_tensor = torch.tensor([targets])
        # 当前合同不包含的条款的index按预测分数的排序
        predicted_scores = self._model_1.get_test_scores(input_items_tensor, input_masks_tensor, targets_tensor)
        top_rank_indices = predicted_scores.squeeze(dim=0).argsort(dim=0, descending=True)[:topN].tolist()
        top_rank_item_idices = [targets[i] for i in top_rank_indices]
        top_rank_item_strs = [self.data_model.itemIdx2Str[i] for i in top_rank_item_idices]
        # print(f"documentStr: new contract")
        # print(f"Missing Clauses(Top{topN}): {top_rank_item_strs}")
        # print('-----------')
        return top_rank_item_strs

    def predict_by_userIdx(self, userIdx, topN=5):
        input_items = self.data_model.userIdx2Items_train[userIdx]
        append_list = [0] * (self.data_model.input_len - len(input_items))
        input_masks = [1] * len(input_items) + append_list
        targets = list(set(range(1, self.data_model.numItem)) - set(input_items))

        input_items_tensor = torch.tensor([input_items + append_list])
        input_masks_tensor = torch.tensor([input_masks])
        targets_tensor = torch.tensor([targets])
        # 当前合同不包含的条款的index按预测分数的排序
        predicted_scores = self._model_1.get_test_scores(input_items_tensor, input_masks_tensor, targets_tensor)
        top_rank_indices = predicted_scores.squeeze(dim=0).argsort(dim=0, descending=True)[:topN].tolist()
        top_rank_item_idices = [targets[i] for i in top_rank_indices]
        top_rank_item_strs = [self.data_model.itemIdx2Str[i] for i in top_rank_item_idices]
        user_str = self.data_model.userIdx2Str[userIdx]
        # print(f"documentStr: {user_str}")
        # print(f"Missing Clauses(Top{topN}): {top_rank_item_strs}")
        # print('-----------')
        return top_rank_item_strs

    @property
    def model(self):
        return self._model_1

    @property
    def optimizer(self):
        return self._optimizer_1


def _get_optimizer(model, learning_rate, weight_decay=0.01):
    param_optimizer = list(model.named_parameters())
    no_decay = ['bias', 'LayerNorm.bias', 'LayerNorm.weight']
    optimizer_grouped_parameters = [
        {'params': [p for n, p in param_optimizer if
                    not any(nd in n for nd in no_decay)], 'weight_decay': weight_decay},
        {'params': [p for n, p in param_optimizer if
                    any(nd in n for nd in no_decay)], 'weight_decay': 0.0}]

    return optim.Adam(optimizer_grouped_parameters, lr=learning_rate)


def set2str(input_set):
    set_str = ''
    set_len = len(input_set)
    for i, item in enumerate(input_set):
        if i < set_len - 1:
            set_str += str(item) + ','
        else:
            set_str += str(item)
    return set_str

