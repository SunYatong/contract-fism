import torch
from torch import nn
from model_utils.BERT_SeqRec import BertModel
from model_utils.BERT_SeqRec import BertConfig
# from model_utils.Utils import normalize

from model.GraphSeq import ContextEncoder


class FISM(ContextEncoder):

    def __init__(self, config):
        super().__init__(config)

    def forward(self, hist_item_ids, masks):
        """
        hist_item_ids = [bs, input_len]
        masks = = [bs, seq_len]
        """
        # [bs, input_len, hidden_size]
        hist_item_embed = self.item_embeddings.weight[hist_item_ids]
        # [bs, input_len, hidden_size]
        # masked_hist_item_embed = hist_item_embed * masks.unsqueeze(2)#原代码
        masked_hist_item_embed = hist_item_embed * masks.unsqueeze(2).type_as(hist_item_embed)
        # [bs, hidden_size]
        # averaged_hist_item_embed = masked_hist_item_embed.sum(dim=1, keepdim=False) / (masks.sum(dim=1, keepdim=True) + 1e-7)#原代码
        averaged_hist_item_embed = masked_hist_item_embed.sum(dim=1, keepdim=False) / (masks.sum(dim=1, keepdim=True) + 1e-7).type_as(masked_hist_item_embed)

        return averaged_hist_item_embed






