import torch


def print_dict(metrics, name):
    print('#' * 10 + ' ' + name + ' ' + '#' * 10)
    for key, value in metrics.items():
        print(f'{key}: {value}')


class RankingEvaluator(object):

    def __init__(self, test_batches):

        self.best_metrics = {
            'precision_1:': 0,
            'precision_5:': 0,
            'precision_10:': 0,
            'precision_20:': 0,
            'recall_1:': 0,
            'recall_5:': 0,
            'recall_10:': 0,
            'recall_20:': 0,
        }
        self.best_iter = 0
        self.test_batches = test_batches

    def evaluate(self, model, train_iter, userIdx2target_num, verbose=True):
        model.eval()
        current_metrics = {
            'precision_1:': 0,
            'precision_5:': 0,
            'precision_10:': 0,
            'precision_20:': 0,
            'recall_1:': 0,
            'recall_5:': 0,
            'recall_10:': 0,
            'recall_20:': 0,
        }
        # evaluate each batch
        test_size = 0
        for test_batch in self.test_batches:
            batch_metrics_values = model.eval_ranking(test_batch, userIdx2target_num)
            for metric, value in batch_metrics_values.items():
                current_metrics[metric] += value
            test_size += test_batch[0].shape[0]
        # summarize the metrics and update the best metrics
        for metric, value in current_metrics.items():
            avg_value = value / test_size #原代码
            # avg_value = value / (test_size+1) # batch从0开始?
            current_metrics[metric] = avg_value
            if avg_value > self.best_metrics[metric]:
                self.best_metrics[metric] = avg_value
                self.best_iter = train_iter
        # print results
        if verbose:
            print_dict(current_metrics, 'current_metrics')
            print_dict(self.best_metrics, 'best_metrics')
        # early stop
        best_gap = train_iter - self.best_iter
        if best_gap > 40:
            if verbose:
                print('early stop')
            return False, current_metrics['precision_10:']
        else:
            if verbose:
                print(f'no increase in recent {best_gap} iters')
            return True, current_metrics['precision_10:']
