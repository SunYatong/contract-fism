import fitz
import os

# filePath = 'C:\\Users\\lxj\\Desktop\\合同数据集' #原始的合同文件 目录需要改
# outPath = 'C:\\Users\\lxj\\Desktop\\preprocess' #处理后的文件  目录需要改
#读每一个pdf高亮
def _parse_highlight(annot, wordlist):
    points = annot.vertices
    #print(points)
    quad_count = int(len(points) / 4)
    sentences = ['' for i in range(quad_count)]
    for i in range(quad_count):
        r = fitz.Quad(points[i * 4: i * 4 + 4]).rect
        words = [w for w in wordlist if fitz.Rect(w[:4]).intersects(r)]
        sentences[i] = ' '.join(w[4] for w in words)
    sentence = ' '.join(sentences)
    return sentence

#读一个文件
def read_contract(filename):
    all_pairs=[] #[条款名，... ，条款名]
    mupdf_doc = fitz.open(filename)  #目录需要改
    pages = mupdf_doc.page_count
    # for mupdf_page in mupdf_doc:
    for page in range(pages):
        mupdf_page = mupdf_doc.load_page(page)
        wordlist = mupdf_page.get_text("words")  # list of words on page
        wordlist.sort(key=lambda w: (w[3], w[0]))  # ascending y, then x
        #print(wordlist)
        for annot in mupdf_page.annots():
            # underline / highlight / strikeout / squiggly : 8 / 9 / 10 / 11
            if annot.type[0] == 8:
                item=_parse_highlight(annot, wordlist)
                all_pairs.append(item)
    return all_pairs

#读目录下的多个文件
# #读整个合同的条款，保存格式为[文件名，条款名]
# all_pairs=[] #[文件名，条款名]
# for k in os.listdir(filePath):
#
#     mupdf_doc = fitz.open(os.path.join(filePath,k))
#     pages = mupdf_doc.page_count
#     # for mupdf_page in mupdf_doc:
#     for page in range(pages):
#         mupdf_page = mupdf_doc.load_page(page)
#         wordlist = mupdf_page.get_text("words")  # list of words on page
#         wordlist.sort(key=lambda w: (w[3], w[0]))  # ascending y, then x
#         #print(wordlist)
#         for annot in mupdf_page.annots():
#             # underline / highlight / strikeout / squiggly : 8 / 9 / 10 / 11
#             if annot.type[0] == 8:
#                 item=_parse_highlight(annot, wordlist)
#                 all_pairs.append([k,item])
#
# #写入文件
# with open(os.path.join(outPath,"11.txt"),"w", encoding='utf-8') as f:
#     print(all_pairs,file=f)